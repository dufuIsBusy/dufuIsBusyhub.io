## mysql笔记

#### 启动mysql :
	mysqld --console
#### 关闭mysql :
	mysqladmin -u root shutdown
#### Install the server as a service using this command :
	mysqld [--install  | --install-manual [MySQL]]
	mysqld [--install  | --install-manual [MySQL]]
#### 启动服务停止服务 :
- NET START MySQL
- NET STOP MySQL
#### 移除服务 :
	mysqld --remove
#### Using Option Files :
	[Using Option Files](http://dev.mysql.com/doc/refman/5.6/en/option-files.html)
#### 终极解决方案 :
	mysqld --verbose --help
#### Testing The MySQL Installation :
* mysqlshow
* mysqlshow -u root mysql
* mysqladmin version status proc

#### Connecting to the MySQL Server :
*	mysql
	This command invokes mysql without specifying any connection parameters explicitly:
	Because there are no parameter options, the default values apply:

	- The default host name is localhost. On Unix, this has a special meaning, as described later.

	- The default user name is ODBC on Windows or your Unix login name on Unix.

	- No password is sent if neither -p nor --password is given.

	For mysql, the first nonoption argument is taken as the name of the default database. If there is no such option, mysql does not select a default database.

* 	mysql --host=localhost --user=myname --password=mypass mydb
	mysql -h localhost -u myname -pmypass mydb
*	mysql -uroot -p mydb
#### 查看MySQL的当前用户 :
	SELECT USER();
#### 查看所有用户 :
	SELECT user,host,password FROM mysql.user;

#### 增加新用户 :
	grant ALL PRIVILEGES on *.* to 'envesgo_oa'@'192.168.0.132'
	identified by 'envesgo';

#### 授权任何用户访问数据库 :
	grant ALL PRIVILEGES on *.* to 'envesgo_oa'@'%'identified by 'envesgo';
	flush privileges;

	select user ,password , host from mysql.user;

#### 删除mysql的匿名账户，新添加的用户才起作用
	delete from user where user='';
	flush privileges;

####Handling Connection Errors
*Important*
If you are using multilanguage databases then you must specify the character set in the connection string. If you do not specify the character set, the connection defaults to the latin1 charset. You can specify the character set as part of the connection string, for example:
	MySqlConnection myConnection = new MySqlConnection("server=127.0.0.1;uid=root;" +
    	"pwd=12345;database=test;Charset=latin1;");

- 0: Cannot connect to server.

- 1045: Invalid user name and/or password.

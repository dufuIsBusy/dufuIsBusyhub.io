## sublime text 快捷键	:heart:
- 复制当前行内容到下一行：<kbd>Ctrl + shift + D</kbd>
- 移动当前行内容到下一行：<kbd>Ctrl + shift + ↓</kbd>
- 将当前屏幕分为上下左右四屏：<kbd>Alt + shift + 5</kbd>
- 选中光标所在位置的单词：<kbd>Ctrl + D</kbd> + <kbd>Alt + F3</kbd>(全选批量修改)
- 选中当前行 <kbd>Ctrl + L</kbd>
- 在起始括号和结尾括号间切换 <kbd>Ctrl + M</kbd>
- 选择括号间的内容 <kbd>Ctrl + Shift + M</kbd>
- 删除当前整行内容 <kbd> Ctrl + Shift + K </kbd>
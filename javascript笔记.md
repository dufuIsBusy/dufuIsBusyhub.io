 /**
 * @Description: Description
 * @Author:      dfg
 * @DateTime:    2015-05-22 15:12:39
 */

1、recursiveObject
//循环遍历一个对象及其子对象的属性和值
        function recursiveObject(obj) {            

            for(var o in obj) {
              if(obj.hasOwnProperty(o)) {
                  if(typeof obj[o] == "object") {                  
                    recursiveObject(obj[o]);
                  } else {                 
                    console.log(o+"="+obj[o]);
                  }
              }                                
                
            }
        }

/**
 * javascipt调试方法
 */
2、javascript加断点调试
debugger;